<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="header.xsl" />
	<xsl:include href="senderReceiver.xsl" />
	<xsl:include href="mailReason.xsl" />
	<xsl:include href="footer.xsl" />
	<xsl:include href="style.xsl" />
	<xsl:template match="/">
		<html>
			<head>
				<xsl:call-template name="generalStyle" />
			</head>
				<body>
				<xsl:attribute name="style">
					<xsl:call-template name="bodyStyleCss" /> <!-- style.xsl -->
				</xsl:attribute>
				<xsl:call-template name="head" /> <!-- header.xsl -->
				<xsl:call-template name="senderReceiver" /> <!-- SenderReceiver.xsl -->
				<br />
				<br />
				<xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
				<table cellspacing="0" cellpadding="5" border="0">
					<tr>
						<td>
							<h>@@inform_you_item_below@@ </h>
							<h>@@borrowed_by_you@@ <a href="https://csusm-primo.hosted.exlibrisgroup.com/primo-explore/account?institution=01CALS_USM&amp;vid=01CALS_USM&amp;lang=en_US&amp;section=overview"> renew online </a>  @@decalred_as_lost@@</h>
						</td>
					</tr>
				</table>
				<table cellpadding="5" class="listing">
					<xsl:attribute name="style">
						<xsl:call-template name="mainTableStyleCss" /> <!-- style.xsl -->
					</xsl:attribute>
					<xsl:for-each select="notification_data">
					<table>
						<tr>
							<td>
							<b>@@lost_item@@ :</b> <xsl:value-of select="item_loan/title"/>
							<br />
							<b>@@description@@ :</b><xsl:value-of select="item_loan/description"/>
							<br />
							<b> @@by@@ :</b><xsl:value-of select="item_loan/author"/>
							<br />
							<b>@@library@@ :</b><xsl:value-of select="organization_unit/name"/>
							<br />
							<b>@@loan_date@@ :</b><xsl:value-of select="item_loan/loan_date"/>
							<br />
							<b>@@due_date@@ :</b><xsl:value-of select="item_loan/due_date"/>
							<br />
							<b>@@barcode@@ :</b><xsl:value-of select="item_loan/barcode"/>
							<br />
							<b>@@call_number@@ :</b><xsl:value-of select="phys_item_display/call_number"/>
							<br /><br />
							<b>@@charged_with_fines_fees@@ </b>
							</td>
						</tr>
					</table>
					</xsl:for-each>
					<br />
					@@additional_info_1@@
					@@additional_info_2@@
					<br />
					<table>
						<tr><td>@@sincerely@@</td></tr>
						<tr><td><br /><br />@@department@@</td></tr>
					</table>
				</table>
				<br />
				<!-- footer.xsl -->
				<xsl:call-template name="myAccount" />
				<xsl:call-template name="contactUs" />
				<xsl:call-template name="lastFooter" /> 
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>