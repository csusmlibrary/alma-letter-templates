<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- Default
<xsl:template match="/">
  <xsl:copy-of select="/" />
</xsl:template>

</xsl:stylesheet>
-->

<xsl:template match="/">
<xsl:for-each select="userRecords/userRecord">
<xsl:value-of select="concat(userDetails/userName,',&quot;',userDetails/lastName,',&#160;',userDetails/firstName,'&quot;,')"/>
<!-- attempt to cut out hardcoded NONE for dynamic  ,'NONE',',',userDetails/userGroup,',','' )"/>
-->
<xsl:for-each select="userBlockList">
<xsl:if test="userBlock">BLOCKED,</xsl:if>
<xsl:if test="not(userBlock)">NONE,</xsl:if>
</xsl:for-each>

<xsl:value-of select="concat(userDetails/userGroup,',')"/>

<xsl:for-each select="userAddressList">
<xsl:if test="userEmail[@preferred = 'true']">
<xsl:value-of select="userEmail/email"/>
</xsl:if>
</xsl:for-each>
<xsl:value-of select="concat(',',userDetails/expiryDate,',')"/>


<!-- start of UNIV_ID test  -->
<xsl:for-each select="userIdentifiers/userIdentifier">
<xsl:if test="type[contains(text(),'UNIV_ID')]">
<xsl:value-of select="concat(value)"/>
</xsl:if>
</xsl:for-each>
<!-- end of UNIV_ID test -->
<!-- start of OTHER_ID_1 (legacy barcode test)  REMOVED 11/05 as Legacy Barcodes no longer used - SE
<xsl:for-each select="userIdentifiers/userIdentifier">
<xsl:if test="type[contains(text(),'OTHER_ID_1')]">
<xsl:value-of select="concat(',',value)"/>
</xsl:if>
</xsl:for-each>
end of OTHER_ID_1 test -->
<xsl:text>&#xa;</xsl:text>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>